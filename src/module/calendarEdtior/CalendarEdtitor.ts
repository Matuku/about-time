import { calendars } from "../calendar/DTCalc";
import { DateTime } from  "../calendar/DateTime";
import { DTCalc } from  "../calendar/DTCalc";
import { ElapsedTime } from "../ElapsedTime";

export class CalendarEditor extends FormApplication {
  constructor(calendarSpec = calendars[Object.keys(calendars)[game.settings.get("about-time", "calendar")]] , options: FormApplicationOptions) {
    // check for calendar-weather being loaded.
    super(duplicate(calendarSpec), options);
  }

  static get defaultOptions() {
    const options = super.defaultOptions;
    options.template = "modules/about-time/templates/calendarEdtior.html";
    options.width = 550;
   // options.height = 520; // should be "auto", but foundry has problems with dynamic content
    options.title = "Calendar Editor";
    options.editable = game.user.isGM;
    return options;
  }

  getData() {
    let data: any = super.getData();
    let now = DateTime.now();
    data.now = now.longDate();
    data.month_keys = Object.keys(this.object.month_len)
    data.months = Object.keys(data.object.month_len).map(name => {
        return {
          name: name,
          days0: data.object.month_len[name].days[0], 
          days1: data.object.month_len[name].days[1], 
          intercalary: data.object.month_len[name].intercalary
        }
      });
    data.DateYears = now.years;
    data.DateMonths = now.months + 1;
    data.DateDays = now.days + 1;
    data.TimeHours = now.hours;
    data.TimeMinutes = now.minutes;
    data.TimeSeconds = now.seconds;
    data.dow = now.dow();
    console.log("Calendar editor get data returning ", data)
    return data;
  }

  async _updateObject(event, formData) {
    let new_month_len = {};

    formData["months.name"].forEach((name, index) => {
      new_month_len[name] = {days: [parseInt(formData["months.days0"][index]), parseInt(formData["months.days1"][index])], intercalary: formData["months.intercalary"][index]};
    })

    console.log(new_month_len)
    this.object.month_len = new_month_len;
    this.object.weekdays = formData["dayname"];
    this.object.hours_per_day = parseInt(formData.hours_per_day);
    this.object.minutes_per_hour = parseInt(formData.minutes_per_hour);
    this.object.seconds_per_minute = parseInt(formData.seconds_per_minute);
    this.object.has_year_0 = formData.has_year_0;
    //TODO leap year rules

    DTCalc.saveUserCalendar(this.object);
    DTCalc.userCalendarChanged();

    let newDateTime = DateTime.create({
      years: parseInt(formData.DateYears), months: parseInt(formData.DateMonths) - 1, days: parseInt(formData.DateDays) - 1, 
      hours: parseInt(formData.TimeHours), minutes: parseInt(formData.TimeMinutes), seconds: parseInt(formData.TimeSeconds)
    })
    ElapsedTime.setDateTime(newDateTime);
    newDateTime.setCalDow(parseInt(formData.dow));
    this.object.first_day = DTCalc.firstDay;
    DTCalc.saveUserCalendar(this.object);

    game.settings.set("about-time", "calendar", 0);
    DTCalc.userCalendarChanged();
    ElapsedTime._save(true);
  }

  activateListeners(html) {
    super.activateListeners(html);
    if (!game.user.isGM) return;

    html.find('.month-delete').click(async ev => {
      let li = $(ev.currentTarget)
      let id = li.attr("month-id");
      delete this.object.month_len[id];
      console.log("inside handler ", li, this, id)
      li.slideUp(200);
      this.render(false);
    });
    

    html.find('.month-create').click(async ev => {
      let newMonthName = "New Month " + Object.keys(this.object.month_len).length;
      this.object.month_len[newMonthName] = {days: [1,1], intercalary: false};
      this.render(false);
    });

    html.find('.weekday-delete').click(async ev => {
      let li = $(ev.currentTarget).parents(".weekday");
      let id = Number(li.attr("weekday-id"));
      this.object.weekdays.splice(id,1)
      li.slideUp(200);
      this.render(false);
    });

    html.find('.weedkay-create').click(async ev => {
      let newMonthName = "New Day " + this.object.weekdays.length;
      this.object.weekdays.push(newMonthName);
      this.render(false);
    });

  }
}